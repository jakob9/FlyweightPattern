﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight
{
    class Bomb : IBomb
    {
        private int _damage;

        public Bomb(int damage)
        {
            _damage = damage;
        }

        public void BlowUp(int x, int y)
        {
            Console.WriteLine("Blowing up with {0} damage at ({1}, {2})", _damage, x, y);
        }
    }
}
