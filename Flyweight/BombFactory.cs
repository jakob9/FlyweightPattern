﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;

namespace Flyweight
{
    class BombFactory
    {
        private readonly IDictionary<string, IBomb> _bombs 
            = new ConcurrentDictionary<string, IBomb>();

        public int InstanceCount { get { return _bombs.Count; } }


        public IBomb GetBomb(string key)
        {
            switch (key)
            {
                case "DirtyBomb":
                    return GetDirtyBomb();
                case "PipeBomb":
                    return GetPipeBomb();
                default:
                    throw new Exception("Unknown key: " + key);
            }
        }

        private IBomb GetDirtyBomb()
        {
            if(!_bombs.ContainsKey("DirtyBomb"))
                _bombs["DirtyBomb"] = new Bomb(999999);

            return _bombs["DirtyBomb"];
        }

        private IBomb GetPipeBomb()
        {
            if (!_bombs.ContainsKey("PipeBomb"))
                _bombs["PipeBomb"] = new Bomb(10);

            return _bombs["PipeBomb"];
        }
    }
}
