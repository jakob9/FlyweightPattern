﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flyweight
{
    interface IBomb
    {
        void BlowUp(int x, int y);
    }
}
