﻿using System;
using System.Collections.Generic;

namespace Flyweight
{
    class Program
    {
        private static int REFERENCE_COUNT = 100000000;

        static void Main(string[] args)
        {
            var bombs = new List<IBomb>();
            var factory = new BombFactory();

            Console.WriteLine("Retrieving " + REFERENCE_COUNT + " instances of IBomb");

            for (var i = 0; i < REFERENCE_COUNT; i++)
            {
                var key = (i%2 == 0) ? "DirtyBomb" : "PipeBomb";
                var bomb = factory.GetBomb(key);

                bombs.Add(bomb);
            }

            Console.WriteLine("Total references retrieved: " + bombs.Count);
            Console.WriteLine("Total instances of IBomb in BombFactory: " + factory.InstanceCount);
        }
    }
}
