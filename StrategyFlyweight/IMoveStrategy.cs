﻿
namespace StrategyFlyweight
{
    interface IMoveStrategy
    {
        void MoveTo(int x, int y);
    }
}
