﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyFlyweight
{
    enum MoveStrategy
    {
        Run,
        Walk,
        Crawl
    };

    interface IMoveStrategyFactory
    {
        IMoveStrategy GetMoveStrategy(MoveStrategy key);
    }
}
