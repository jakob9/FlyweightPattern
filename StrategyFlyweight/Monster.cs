﻿
namespace StrategyFlyweight
{
    class Monster
    {
        private int _health;
        private IMoveStrategy _strategy;
        private IMoveStrategyFactory _factory;

        public int Health { get { return _health; } }


        public Monster(IMoveStrategyFactory factory)
        {
            _health = 100;
            _factory = factory;
            UpdateStrategy();
        }

        public void MoveTo(int x, int y)
        {
            _strategy.MoveTo(x, y);
        }

        public void TakeDamage(int damage)
        {
            _health -= damage;
            UpdateStrategy();
        }

        private void UpdateStrategy()
        {
            if (_health < 20)
                _strategy = _factory.GetMoveStrategy(MoveStrategy.Crawl);
            else if (_health < 40)
                _strategy = _factory.GetMoveStrategy(MoveStrategy.Walk);
            else
                _strategy = _factory.GetMoveStrategy(MoveStrategy.Run);
        }
    }
}
