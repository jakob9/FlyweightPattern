﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyFlyweight
{
    class CrawlStrategy : IMoveStrategy
    {
        public void MoveTo(int x, int y)
        {
            Console.WriteLine("Crawling to ({0}, {1})", x, y);
        }
    }
}
