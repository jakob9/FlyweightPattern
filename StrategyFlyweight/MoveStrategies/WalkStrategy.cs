﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StrategyFlyweight
{
    class WalkStrategy : IMoveStrategy
    {
        public void MoveTo(int x, int y)
        {
            Console.WriteLine("Walking to ({0}, {1})", x, y);
        }
    }
}
