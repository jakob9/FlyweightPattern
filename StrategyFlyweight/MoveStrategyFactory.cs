﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using StrategyFlyweight;

namespace StrategyFlyweight
{
    class MoveStrategyFactory : IMoveStrategyFactory
    {
        private readonly IDictionary<string, IMoveStrategy> _strategies 
            = new ConcurrentDictionary<string, IMoveStrategy>();

        public int InstanceCount { get { return _strategies.Count; } }


        public IMoveStrategy GetMoveStrategy(MoveStrategy key)
        {
            switch (key)
            {
                case MoveStrategy.Run:
                    return GetRunStrategy();
                case MoveStrategy.Walk:
                    return GetWalkStrategy();
                case MoveStrategy.Crawl:
                    return GetCrawlStrategy();
                default:
                    throw new Exception("Unknown key: " + key);
            }
        }

        private IMoveStrategy GetRunStrategy()
        {
            if(!_strategies.ContainsKey("Run"))
                _strategies["Run"] = new RunStrategy();

            return _strategies["Run"];
        }

        private IMoveStrategy GetWalkStrategy()
        {
            if (!_strategies.ContainsKey("Walk"))
                _strategies["Walk"] = new WalkStrategy();

            return _strategies["Walk"];
        }

        private IMoveStrategy GetCrawlStrategy()
        {
            if (!_strategies.ContainsKey("Crawl"))
                _strategies["Crawl"] = new CrawlStrategy();

            return _strategies["Crawl"];
        }
    }
}
