﻿using System;
using System.Collections.Generic;

namespace StrategyFlyweight
{
    class Program
    {
        static int NUM_MONSTERS = 100;


        static void Main(string[] args)
        {
            var factory = new MoveStrategyFactory();
            var monsters = new List<Monster>(NUM_MONSTERS);

            for (var i = 0; i < NUM_MONSTERS; i++)
                monsters.Add(new Monster(factory));

            Simulate(monsters);

            Console.WriteLine("Total number of instances of strategies created: " + factory.InstanceCount);
        }

        static void Simulate(IList<Monster> monsters)
        {
            var rnd = new Random();
            var anyoneAlive = true;

            while (anyoneAlive)
            {
                anyoneAlive = false;

                foreach (var monster in monsters)
                {
                    if (monster.Health > 0)
                    {
                        anyoneAlive = true;
                        monster.MoveTo(10, 10);
                        monster.TakeDamage(rnd.Next(1, 75));
                    }
                }
            }
        }
    }
}
